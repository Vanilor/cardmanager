<?php


namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class Api extends Controller
{

    public const DEFAULT_STATUS = "OK";

    /**
     * @param $status
     * @param $code
     * @param $response
     * @return JsonResponse
     *
     */
    protected function makeResponse($status, $code, $response)
    {

        return Response::json([
            "status" => $status,
            "code" => $code,
            "message" => $response,
        ], $code);

    }

}
