<?php


namespace App\Http\Controllers;

use App\Models\Deck;
use Illuminate\Http\Request;

class DeckManagerAPI extends Api implements IApi
{

    private string $id;
    private $deck;

    public function __construct(Request $request)
    {
        $this->id = $request->route("id");
        $this->deck = Deck::where("id", "=", $this->id)->first();
    }

    public function full(){

        if(!$this->allowRequest())
            return $this->makeResponse("not_found", 404, []);

        return $this->makeResponse($this::DEFAULT_STATUS, 200, [
            "id" => $this->id,
            "response_count" => count($this->deck["responses"]),
            "call_count" => count($this->deck["calls"]),
        ]);

    }

    public function responses(){

        if(!$this->allowRequest())
            return $this->makeResponse("not_found", 404, []);

        return $this->makeResponse($this::DEFAULT_STATUS, 200, [
            $this->deck->responses
        ]);

    }

    public function calls(){

        if(!$this->allowRequest())
            return $this->makeResponse("not_found", 404, []);

        return $this->makeResponse($this::DEFAULT_STATUS, 200, [
            $this->deck->calls
        ]);

    }

    public function cards(){

        if(!$this->allowRequest())
            return $this->makeResponse("not_found", 404, []);


        return $this->makeResponse($this::DEFAULT_STATUS, 200, [
            "responses" => $this->deck->responses,
            "calls"     => $this->deck->calls,
        ]);

    }

    public function allowRequest(){

        if(!$this->deck)
            return false;

        return true;
    }

}
