<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::name("deck.")
    ->prefix("/decks/{id}")
    ->group(function () {

        Route::get("/", "DeckManagerAPI@full")->name("fullDeck");
        Route::get("/responses", "DeckManagerAPI@responses")->name("responses");
        Route::get("/calls", "DeckManagerAPI@calls")->name("responses");
        Route::get("/cards", "DeckManagerAPI@cards")->name("cards");

    });
